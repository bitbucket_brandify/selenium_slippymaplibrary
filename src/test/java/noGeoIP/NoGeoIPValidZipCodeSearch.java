package noGeoIP;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class NoGeoIPValidZipCodeSearch extends BrowserStackTestNGTest{
    ExtentTest test14;
	
    @Test
    public void NogeoCoding_Zip_Code_Search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test14=extent.startTest("TC 14 NogeoCoding - Valid ZipCode Search.");
			System.out.println("TC 14 NogeoCoding - Valid ZipCode Search.");
			test14.log(LogStatus.INFO, "Scenario: On valid address , location is found.");
			System.out.println("Scenario: On valid address , location is found.");
			test14.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.nogeoipUrl);
			test14.log(LogStatus.INFO, "Enter invalid zipcode '32801' in address line.");
			System.out.println("Enter invalid zipCode '32801' in address line.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("32801");
			test14.log(LogStatus.INFO, "Selected radius 100 miles.");
			System.out.println("Selected radius 100 miles.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test14.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			try {
				wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Disney World")));
				test14.log(LogStatus.PASS, "Pass."+""+test14.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (Exception e) {
				
				test14.log(LogStatus.FAIL, e.getMessage()+""+test14.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();

			}
			
			test14.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test14.log(LogStatus.ERROR, e.getMessage()+""+test14.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test14);
    }
    
    
}
