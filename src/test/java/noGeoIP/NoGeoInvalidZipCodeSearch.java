package noGeoIP;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class NoGeoInvalidZipCodeSearch extends BrowserStackTestNGTest{
    ExtentTest test12;
	
    @Test
    public void NogeoCoding_invalid_zip_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test12=extent.startTest("TC 12 NogeoCoding - Invalid ZipCode Search.");
			System.out.println("TC 12 NogeoCoding - Invalid ZipCode Search.");
			test12.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if for invalid address search.");
			System.out.println("Scenario: Light Window Pop Up must shows if for invalid address search.");
			test12.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.nogeoipUrl);
			test12.log(LogStatus.INFO, "Enter invalid zipcode '82835' in address line.");
			System.out.println("Enter invalid zipCode '82835' in address line.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("82835");
			test12.log(LogStatus.INFO, "Selected radius 100 miles.");
			System.out.println("Selected radius 100 miles.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test12.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.lightwindow_close));
			try {
				Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), 
						"No locations were found using your search criteria [ 82835 ]. Please try another input address to search for locations.");
				test12.log(LogStatus.PASS, "Pass."+""+test12.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (AssertionError e) {
				test12.log(LogStatus.FAIL, e.getMessage()+""+test12.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test12.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test12.log(LogStatus.ERROR, e.getMessage()+""+test12.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test12);
    }
    
    
}
