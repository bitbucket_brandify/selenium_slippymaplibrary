package noGeoIP;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class NoGeoIPSendDirectionViaEmail extends BrowserStackTestNGTest {

	ExtentTest test9;

	@Test
	public void NogeoIp_send_direction_via_email() throws IOException
	{
		try {
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test9=extent.startTest("TC 9 - NoGeoIP - Send Direction Via Email");
			System.out.println("TC 9 - NoGeoIP - Send Direction Via Email");
			test9.log(LogStatus.INFO, "Scenario - Searched direction must be send to input email address.");
			System.out.println("Scenario - Searched direction must be send to input email address.");
			test9.log(LogStatus.INFO, "Open Url - "+WebElementLocator.nogeoipUrl);
			System.out.println("Open Url - "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.nogeoipUrl);
			test9.log(LogStatus.INFO, "Enter state 'fl' in input address text box.");
			System.out.println("Enter state 'fl' in input address text box.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("fl");
			test9.log(LogStatus.INFO, "Select 100 miles in radius.");
			System.out.println("Select 100 miles in radius.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test9.log(LogStatus.INFO, "Click on 'Search' button to get direction.");
			System.out.println("Click on 'Search' button to get direction.");
			loc.clientsearchButton.click();
			test9.log(LogStatus.INFO, "Click on 'Email' link to get window pop up.");
			System.out.println("Click on 'Email' link to get window pop up.");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//a[contains(@url,'/lite?appkey=A2015774-B8E4-11E2-87AC-DC1D003085D0&action="
					+ "emaillocatorstart&template=emaillocatorstart&uid=1078649644')]")).click();
			wc.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("lightwindow_iframe"));
			wc.until(ExpectedConditions.elementToBeClickable(loc.userEmail));
			test9.log(LogStatus.INFO, "Enter user email address 'ssoni@where2getit.com'.");
			System.out.println("Enter user email address 'ssoni@where2getit.com'.");
			loc.userEmail.sendKeys("ssoni@where2getit.com");
			test9.log(LogStatus.INFO, "- Enter user user comment 'Automated comment'.");
			System.out.println("Enter user user comment 'Automated comment'.");
			loc.userComment.sendKeys("Automated User comment.");
			test9.log(LogStatus.INFO, "Click on 'Submit' button.");
			System.out.println("Click on 'Submit' button.");
			loc.submitButton.click();
			try {
				wc.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Confirmation')]"))));
				test9.log(LogStatus.PASS, "Pass."+""+test9.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (Exception e1) {
				test9.log(LogStatus.FAIL, e1.getMessage()+""+test9.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e1.printStackTrace();


			}
			test9.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test9.log(LogStatus.ERROR, e.getMessage()+""+test9.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}        


	}
	@AfterTest
	public void finish()
	{
		extent.endTest(test9);
	}

}
