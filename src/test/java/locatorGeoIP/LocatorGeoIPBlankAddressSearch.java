package locatorGeoIP;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class LocatorGeoIPBlankAddressSearch extends BrowserStackTestNGTest{
    ExtentTest test15;
	
    @Test
    public void geoCoding_blank_address_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test15=extent.startTest("TC 15:Locator geoCoding - Blank Address Search.");
			System.out.println("TC 15:Locator geoCoding - Blank Address Search.");
			test15.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			System.out.println("Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			test15.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.geoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.geoipUrl);
			driver.get(WebElementLocator.geoipUrl);
			test15.log(LogStatus.INFO, "Clear input address field for blank address search.");
			System.out.println("Clear input address field for blank address search.");
			wc.until(ExpectedConditions.elementToBeClickable(loc.clientinputaddress));
			loc.clientinputaddress.clear();
			loc.clientinputaddress.click();
			test15.log(LogStatus.INFO, "Selected radius 100 miles.");
			System.out.println("Selected radius 100 miles.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test15.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.lightwindow_close));
			try {
				Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), "Please enter an address.");
				test15.log(LogStatus.PASS, "Pass."+""+test15.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");
			} catch (AssertionError e) {
				test15.log(LogStatus.FAIL, e.getMessage()+""+test15.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test15.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test15.log(LogStatus.ERROR, e.getMessage()+""+test15.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test15);
    }
    
    
}
