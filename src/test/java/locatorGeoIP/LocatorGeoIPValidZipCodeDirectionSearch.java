package locatorGeoIP;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class LocatorGeoIPValidZipCodeDirectionSearch extends BrowserStackTestNGTest {

	ExtentTest test20;

	@Test
	public void locator_geo_valid_direction_search() throws IOException
	{
		try {
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test20=extent.startTest("TC 20 - Locator geoIP - Valid Zip Code Search.");
			System.out.println("TC 20 - Locator geoIP - Valid Zip Code Search.");
			test20.log(LogStatus.INFO, "Scenario - Searched direction via zip code must have valid start and end direction.");
			System.out.println("Scenario - Searched direction via zip code must have valid start and end direction.");
			test20.log(LogStatus.INFO, "Open Url - "+WebElementLocator.clientSideUrl);
			System.out.println("Open Url - "+WebElementLocator.geoipUrl);
			driver.get(WebElementLocator.geoipUrl);
			test20.log(LogStatus.INFO, "Enter geoip '32801' in input address text box.");
			System.out.println("Enter geoip '32801' in input address text box.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("32801");
			test20.log(LogStatus.INFO, "Select 100 miles in radius.");
			System.out.println("Select 100 miles in radius.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test20.log(LogStatus.INFO, "Click on 'Search' button to get direction.");
			System.out.println("Click on 'Search' button to get direction.");
			loc.clientsearchButton.click();
			try {
				wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Disney World")));
				test20.log(LogStatus.PASS, "Pass."+""+test20.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");
			} catch (Exception e1) {
				test20.log(LogStatus.FAIL, e1.getMessage()+""+test20.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e1.printStackTrace();
			}
			test20.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test20.log(LogStatus.ERROR, e.getMessage()+""+test20.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}        


	}
	@AfterTest
	public void finish()
	{
		extent.endTest(test20);
	}

}
