package locatorGeoIP;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class LocatorGeoIPZipCodeDirectionSearch extends BrowserStackTestNGTest {

	ExtentTest test18;

	@Test
	public void locator_geo_valid_direction_search() throws IOException
	{
		try {
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test18=extent.startTest("TC 18 - Locator geoIP - Valid Direction Search.");
			System.out.println("TC 18 - Locator geoIP - Valid Direction Search.");
			test18.log(LogStatus.INFO, "Scenario - Searched direction must have valid start and end direction.");
			System.out.println("Scenario - Searched direction must have valid start and end direction.");
			test18.log(LogStatus.INFO, "Open Url - "+WebElementLocator.clientSideUrl);
			System.out.println("Open Url - "+WebElementLocator.geoipUrl);
			driver.get(WebElementLocator.geoipUrl);
			test18.log(LogStatus.INFO, "Enter geoip 'FL' in input address text box.");
			System.out.println("Enter geoip 'FL' in input address text box.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("FL");
			test18.log(LogStatus.INFO, "Select 100 miles in radius.");
			System.out.println("Select 100 miles in radius.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test18.log(LogStatus.INFO, "Click on 'Search' button to get direction.");
			System.out.println("Click on 'Search' button to get direction.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Disney World")));
            driver.findElement(By.linkText("Disney World")).click();
            wc.until(ExpectedConditions.elementToBeClickable(loc.clientaddressLine));
            test18.log(LogStatus.INFO, "Enter End Direction search in address line 'Atlanta, GA'.");
			System.out.println("Enter End Direction search in address line 'Atlanta, GA'.");
            loc.clientaddressLine.click();
            loc.clientaddressLine.clear();
            loc.clientaddressLine.sendKeys("Atlanta, GA");
            test18.log(LogStatus.INFO, "Click on 'Go' button.");
			System.out.println("Click on 'Go' button.");
            loc.clientgoButton.click();
            try {
				wc.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(.,'Atlanta')]")));
				test18.log(LogStatus.PASS, "Pass."+""+test18.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");
			} catch (Exception e) {
				test18.log(LogStatus.FAIL, e.getMessage()+""+test18.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
      
			
			test18.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test18.log(LogStatus.ERROR, e.getMessage()+""+test18.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}        


	}
	@AfterTest
	public void finish()
	{
		extent.endTest(test18);
	}

}
