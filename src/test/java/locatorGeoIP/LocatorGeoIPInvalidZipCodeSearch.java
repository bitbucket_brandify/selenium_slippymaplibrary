package locatorGeoIP;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class LocatorGeoIPInvalidZipCodeSearch extends BrowserStackTestNGTest{
    ExtentTest test17;
	
    @Test
    public void locatorgeoCoding_invalid_zip_code_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test17=extent.startTest("TC 17 Locator geoCoding - Invalid Zip Code Search.");
			System.out.println("TC 17 Locator geoCoding - Invalid Zip Code Search.");
			test17.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if for invalid address search.");
			System.out.println("Scenario: Light Window Pop Up must shows if for invalid address search.");
			test17.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.geoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.geoipUrl);
			driver.get(WebElementLocator.geoipUrl);
			test17.log(LogStatus.INFO, "Enter invalid address '110078' in address line.");
			System.out.println("Enter invalid address '110078' in address line.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("110078");
			test17.log(LogStatus.INFO, "Selected radius 100 miles.");
			System.out.println("Selected radius 100 miles.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test17.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.lightwindow_close));
			try {
				Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), 
						"You entered an address which could not be geocoded [ 110078 ]. Please try another input address to search for locations.");
				test17.log(LogStatus.PASS, "Pass."+""+test17.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (AssertionError e) {
				test17.log(LogStatus.FAIL, e.getMessage()+""+test17.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test17.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test17.log(LogStatus.ERROR, e.getMessage()+""+test17.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test17);
    }
    
    
}
