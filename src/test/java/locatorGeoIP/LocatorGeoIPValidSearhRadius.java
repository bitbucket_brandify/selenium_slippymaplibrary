package locatorGeoIP;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class LocatorGeoIPValidSearhRadius extends BrowserStackTestNGTest{
	ExtentTest test19;


	@Test
	public void locator_geoCoding_valid_search_radius() throws InterruptedException, IOException {
		try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test19=extent.startTest("TC 19 Locator geoCoding - Valid Radius Address Search.");
			System.out.println("TC 19 Locator geoCoding - Valid Radius Address Search.");
			test19.log(LogStatus.INFO, "Scenario: Location must be shown if address input fall for selected radius option.");
			System.out.println("Scenario: Light Window Pop Up must shows if for invalid address search.");
			test19.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.geoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.geoipUrl);
			driver.get(WebElementLocator.geoipUrl);
			test19.log(LogStatus.INFO, "Enter  address 'FL' in address line.");
			System.out.println("Enter  address 'FL' in address line.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("FL");
			WebElement mySelectElm = driver.findElement(By.name("searchradius")); 
			Select mySelect= new Select(mySelectElm);
			List<WebElement> options = mySelect.getOptions();
			for (WebElement option : options) {
				if(option.getText().contains("100 miles") || option.getText().contains("select a radius"))
				{
					try {
						option.click();
						Thread.sleep(3000);
						System.out.println("Click on 'Search' button.");
						loc.clientsearchButton.click();
						wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Disney World")));
						test19.log(LogStatus.PASS, "Pass."+""+test19.addScreenCapture(captureScreenMethod(dest)));
						System.out.println("Pass.");

					} catch (Exception e) {
						
						test19.log(LogStatus.FAIL, e.getMessage()+""+test19.addScreenCapture(captureScreenMethod(dest)));
						System.out.println("Fail.");
						e.printStackTrace();

					}
				}
				else
				{
					test19.log(LogStatus.INFO, "Select radius "+option.getText());
					System.out.println("Select radius "+option.getText());
					option.click();
					Thread.sleep(2000);
					test19.log(LogStatus.INFO, "Click on 'Search' button.");
					System.out.println("Click on 'Search' button.");
					loc.clientsearchButton.click();
					wc.until(ExpectedConditions.elementToBeClickable(loc.lightwindow_close));
					try {
						Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), 
								"No locations were found using your search criteria [ FL ]. Please try another input address to search for locations.");
						test19.log(LogStatus.PASS, "Pass."+""+test19.addScreenCapture(captureScreenMethod(dest)));
						System.out.println("Pass.");
						loc.lightwindow_close.click();

					} catch (AssertionError e) {
						test19.log(LogStatus.FAIL, e.getMessage()+""+test19.addScreenCapture(captureScreenMethod(dest)));
						System.out.println("Fail.");
						e.printStackTrace();
						loc.lightwindow_close.click();

					}
				}
			}
			test19.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test19.log(LogStatus.ERROR, e.getMessage()+""+test19.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}


	}

	@AfterTest
	public void finish() {
		extent.endTest(test19);
	}


}
