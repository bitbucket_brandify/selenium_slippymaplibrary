package pageObjectModel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import browserStackTestNG.BrowserStackTestNGTest;

public  class WebElementLocator extends BrowserStackTestNGTest{
	
	public static String clientSideUrl="http://hosted.where2stageit.com/ramtest/clientside";
	public static String ddonlyUrl="http://hosted.where2stageit.com/ramtest/clientside/ddonly.html";
	public static String nogeoipUrl="http://hosted.where2stageit.com/ramtest/nogeoip.html";
	public static String geoipUrl="http://hosted.where2stageit.com/ramtest/";


	
	//ddonly.html
	@FindBy(id="startaddress")
	public
	WebElement startAddress;
	
	@FindBy(id="endaddress")
	public
	WebElement endAddress;
	
	@FindBy(id="driving_directions_button")
	public
	WebElement searchDirectionButton;
	
	@FindBy(id="lightwindow_title_bar_close_link")
	public
	WebElement lightwindow_close;
	
	@FindBy(xpath="//*[@id='senddirectionsto']/a")
	public
	WebElement email;
	
	@FindBy(name="useremail")
	public
	WebElement userEmail;
	
	@FindBy(name="usermobile")
	public
	WebElement userMobile;
	
	@FindBy(name="usercomments")
	public
	WebElement userComment;
	
	@FindBy(className="go_button")
	public
	WebElement submitButton;
	
	//clientSide 
	@FindBy(id="inputaddress")
	public
	WebElement clientinputaddress;
	
	@FindBy(id="search_button")
	public
	WebElement clientsearchButton;
	
	@FindBy(css=".ol-overlay-container input[type='text']")
	public
	WebElement clientaddressLine;
	
	@FindBy(css=".ol-overlay-container input[type='submit']")
	public
	WebElement clientgoButton;
	
	@FindBy(css="#panel > #collection_maneuvers > thead > tr > th > #senddirectionsto")
	public
	WebElement clientdistanceMenu;
	
	@FindBy(xpath="//a[contains(@url,'/lite?appkey=DCD7623A-0CF8-11E4-823A-DEDE746343CC&action=emaillocatorstart&template=emaillocatorstart&uid=1078649643')]")
	public
	WebElement clientSendEmail;
	
	//Mobile Web Element
	@FindBy(id="addressline")
	public
	WebElement maddressLine;
	
	@FindBy(className="textbox")
	public
	WebElement mlightwindow;
}
