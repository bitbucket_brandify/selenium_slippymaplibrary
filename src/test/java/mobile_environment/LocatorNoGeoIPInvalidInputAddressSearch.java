package mobile_environment;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class LocatorNoGeoIPInvalidInputAddressSearch extends BrowserStackTestNGTest{
    ExtentTest test22;
	
    @Test
    public void NogeoCoding_invalid_input_address_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test22=extent.startTest("TC 22 NogeoCoding - Invalid Address Search.");
			System.out.println("TC 22 NogeoCoding - Invalid Address Search.");
			test22.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if for invalid address search.");
			System.out.println("Scenario: Light Window Pop Up must shows if for invalid address search.");
			test22.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.nogeoipUrl);
			test22.log(LogStatus.INFO, "Enter invalid input address 'abc00000' in address line.");
			System.out.println("Enter invalid input address 'abc00000' in address line.");
			loc.maddressLine.click();
			loc.maddressLine.clear();
			loc.maddressLine.sendKeys("abc00000");
		
			Thread.sleep(2000);
			test22.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.mlightwindow));
			try {
				Assert.assertEquals(driver.findElement(By.className("textbox")).getText(), 
						"No locations were found using your search criteria [ abc00000 US ]. Please try another input address to search for locations.");
				test22.log(LogStatus.PASS, "Pass."+""+test22.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (AssertionError e) {
				test22.log(LogStatus.FAIL, e.getMessage()+""+test22.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test22.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test22.log(LogStatus.ERROR, e.getMessage()+""+test22.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test22);
    }
    
    
}
