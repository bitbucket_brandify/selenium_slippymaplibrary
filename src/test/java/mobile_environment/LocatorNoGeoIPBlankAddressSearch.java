package mobile_environment;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LocatorNoGeoIPBlankAddressSearch extends BrowserStackTestNGTest{
    ExtentTest test21;
	
    @Test
    public void m_nogeoIP_blank_address_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test21=extent.startTest("TC 21:Locator NogeoIP - Blank Address Search.");
			System.out.println("TC 21:Locator NogeoIP - Blank Address Search.");
			test21.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			System.out.println("Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			test21.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.geoipUrl);
			test21.log(LogStatus.INFO, "Clear input address field for blank address search.");
			System.out.println("Clear input address field for blank address search.");
			wc.until(ExpectedConditions.elementToBeClickable(loc.maddressLine));
			loc.maddressLine.click();
			loc.maddressLine.clear();
			Thread.sleep(2000);
			test21.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.mlightwindow));
			try {
				assertEquals(driver.findElement(By.className("textbox")).getText(), "Please enter an address.");
				test21.log(LogStatus.PASS, "Pass."+""+test21.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");
			} catch (AssertionError e) {
				test21.log(LogStatus.FAIL, e.getMessage()+""+test21.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test21.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test21.log(LogStatus.ERROR, e.getMessage()+""+test21.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test21);
    }
    
    
}
