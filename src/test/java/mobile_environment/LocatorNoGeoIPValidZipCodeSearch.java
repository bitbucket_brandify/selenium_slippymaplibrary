package mobile_environment;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class LocatorNoGeoIPValidZipCodeSearch extends BrowserStackTestNGTest{
    ExtentTest test24;
	
    @Test
    public void NogeoCoding_Zip_Code_Search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test24=extent.startTest("TC 24 NogeoCoding - Valid ZipCode Search.");
			System.out.println("TC 24 NogeoCoding - Valid ZipCode Search.");
			test24.log(LogStatus.INFO, "Scenario: On valid address , location is found.");
			System.out.println("Scenario: On valid address , location is found.");
			test24.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.nogeoipUrl);
			test24.log(LogStatus.INFO, "Enter valid zipcode '32801' in address line.");
			System.out.println("Enter valid zipCode '32801' in address line.");
			loc.maddressLine.click();
			loc.maddressLine.clear();
			loc.maddressLine.sendKeys("32801");
			
			Thread.sleep(2000);
			test24.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			try {
				wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Disney World")));
				test24.log(LogStatus.PASS, "Pass."+""+test24.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (Exception e) {
				
				test24.log(LogStatus.FAIL, e.getMessage()+""+test24.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();

			}
			
			test24.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test24.log(LogStatus.ERROR, e.getMessage()+""+test24.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test24);
    }
    
    
}
