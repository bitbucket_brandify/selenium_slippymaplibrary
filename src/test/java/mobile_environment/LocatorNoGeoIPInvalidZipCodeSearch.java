package mobile_environment;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class LocatorNoGeoIPInvalidZipCodeSearch extends BrowserStackTestNGTest{
    ExtentTest test23;
	
    @Test
    public void NogeoCoding_invalid_input_address_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test23=extent.startTest("TC 23 NogeoCoding - Invalid ZipCode Search.");
			System.out.println("TC 23 NogeoCoding - Invalid ZipCode Search.");
			test23.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if for invalid zip code search.");
			System.out.println("Scenario: Light Window Pop Up must shows if for invalid zip code search.");
			test23.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.nogeoipUrl);
			driver.get(WebElementLocator.nogeoipUrl);
			test23.log(LogStatus.INFO, "Enter invalid zip code address '82835' in address line.");
			System.out.println("Enter invalid zip code '82835' in address line.");
			loc.maddressLine.click();
			loc.maddressLine.clear();
			loc.maddressLine.sendKeys("82835");

			Thread.sleep(2000);
			test23.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.mlightwindow));
			try {
				Assert.assertEquals(driver.findElement(By.className("textbox")).getText(), 
						"No locations were found using your search criteria [ 82835 US ]. Please try another input address to search for locations.");
				test23.log(LogStatus.PASS, "Pass."+""+test23.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (AssertionError e) {
				test23.log(LogStatus.FAIL, e.getMessage()+""+test23.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test23.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test23.log(LogStatus.ERROR, e.getMessage()+""+test23.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test23);
    }
    
    
}
