package ddOnly;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class DirectionBlankAddressSearch extends BrowserStackTestNGTest {

	ExtentTest test1;

	@Test
	public void check_for_blank_address_search() throws IOException
	{
		try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test1=extent.startTest("TC 1-Client Side - Direction - Blank Address");
			System.out.println("TC 1-Client Side - Direction - Blank Address");
			test1.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			System.out.println("Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			test1.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			driver.get(WebElementLocator.ddonlyUrl);
			test1.log(LogStatus.INFO, "Test Step 2 - Enter geoIP 84321 in 'End address'.");
			System.out.println("Test Step 2 - Enter geoIP 84321 in 'End address'.");
			loc.endAddress.click();
			loc.endAddress.clear();
			loc.endAddress.sendKeys("84321");
			test1.log(LogStatus.INFO, "Test Step 3 - Click on 'Get Direction' button.");
			System.out.println("Test Step 3 - Click on 'Get Direction' button.");
			loc.searchDirectionButton.click();
			wc.until(ExpectedConditions.elementToBeClickable(loc.lightwindow_close));
			try {
				Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), "Driving directions could not be generated");
				test1.log(LogStatus.PASS, "Pass.");
				System.out.println("Pass."+test1.addScreenCapture(captureScreenMethod(dest)));
			} catch (AssertionError e) {
				test1.log(LogStatus.FAIL, e.getMessage()+""+test1.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			loc.lightwindow_close.click();
			test1.log(LogStatus.INFO, "Done.");
			System.out.println("Done");
		} catch (Exception e) {
			test1.log(LogStatus.ERROR, e.getMessage());
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}
	}

	@AfterTest
	public void finish()
	{
		extent.endTest(test1);
	}

}
