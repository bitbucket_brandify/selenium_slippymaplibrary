package ddOnly;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class DirectionSendDirectionViaEmail extends BrowserStackTestNGTest {

	ExtentTest test3;

	@Test
	public void check_for_direction_sen_via_email() throws IOException
	{
		try {
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test3=extent.startTest("TC 3 - Client Side - Direction - Send Search Direction via Email.");
			System.out.println("TC 3 - Client Side - Direction - Send Search Direction via Email.");
			test3.log(LogStatus.INFO, "Scenario: Check searched direction send to email address provided.");
			System.out.println("Scenario: Check searched direction send to email address provided.");
			test3.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			driver.get(WebElementLocator.ddonlyUrl);
			test3.log(LogStatus.INFO, "Test Step 2 - Enter geoIP 78701 in 'Start address'.");
			System.out.println("Test Step 2 - Enter geoIP 78701 in 'Start address'.");
			loc.startAddress.click();
			loc.startAddress.clear();
			loc.startAddress.sendKeys("78701");
			test3.log(LogStatus.INFO, "Test Step 3 - Enter geoIP 84321 in 'End address'.");
			System.out.println("Test Step 3 - Enter geoIP 84321 in 'End address'.");
			loc.endAddress.click();
			loc.endAddress.clear();
			loc.endAddress.sendKeys("84321");
			test3.log(LogStatus.INFO, "Test Step 4 - Click on 'Get Direction' button.");
			System.out.println("Test Step 4 - Click on 'Get Direction' button.");
			loc.searchDirectionButton.click();

			test3.log(LogStatus.INFO, "Test Step 5 - Click on 'email' link.");
			System.out.println("Test Step 5 - Click on 'email' link.");
			Thread.sleep(5000);
			List<WebElement> li = driver.findElements(By.xpath(".//*[@id='senddirectionsto']/a"));
			li.get(1).click();
			wc.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("lightwindow_iframe"));
			wc.until(ExpectedConditions.elementToBeClickable(loc.userEmail));
			test3.log(LogStatus.INFO, "Test Step 6 - Enter user email address 'ssoni@where2getit.com'.");
			System.out.println("Test Step 6 - Enter user email address 'ssoni@where2getit.com'.");
			loc.userEmail.sendKeys("ssoni@where2getit.com");
			test3.log(LogStatus.INFO, "Test Step 7 - Enter user user comment 'Automated comment'.");
			System.out.println("Test Step 7 - Enter user user comment 'Automated comment'.");
			loc.userComment.sendKeys("Automated User comment.");
			test3.log(LogStatus.INFO, "Test Step 8 - Click on 'Submit' button.");
			System.out.println("Test Step 8 - Click on 'Submit' button.");
			loc.submitButton.click();
			try {
				wc.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Confirmation')]"))));
				test3.log(LogStatus.PASS, "Pass."+""+test3.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (Exception e1) {
				test3.log(LogStatus.FAIL, e1.getMessage()+""+test3.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e1.printStackTrace();


			}
			test3.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test3.log(LogStatus.ERROR, e.getMessage()+""+test3.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}
	}

	@AfterTest
	public void finish()
	{
		extent.endTest(test3);
	}

}
