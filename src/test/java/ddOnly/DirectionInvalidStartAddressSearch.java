package ddOnly;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class DirectionInvalidStartAddressSearch extends BrowserStackTestNGTest {

	ExtentTest test2;

	@Test
	public void check_for_invalid_direction_search() throws IOException
	{
		try {
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test2=extent.startTest("TC 2 - Client Side - Direction - Invalid Start Address Search.");
			System.out.println("TC 2 - Client Side - Direction - Invalid Start Address Search.");
			test2.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if 'Invalid Start Address' is not provided.");
			System.out.println("Scenario: Light Window Pop Up must shows if 'Invalid Start Address' is not provided.");
			test2.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			driver.get(WebElementLocator.ddonlyUrl);
			test2.log(LogStatus.INFO, "Test Step 2 - Enter invalid start address 'abc23000'.");
			System.out.println("Test Step 2 - Enter invalid start address 'abc23000'.");
			loc.startAddress.click();
			loc.startAddress.clear();
			loc.startAddress.sendKeys("abc23000");
			test2.log(LogStatus.INFO, "Test Step 3 - Enter '84321' in end address.");
			System.out.println("Test Step 3 - Enter '84321' in end address.");
			loc.endAddress.click();
			loc.endAddress.clear();
			loc.endAddress.sendKeys("84321");
			test2.log(LogStatus.INFO, "Test Step 4 - Click on 'Get Direction' button.");
			System.out.println("Test Step 4 - Click on 'Get Direction' button.");
			loc.searchDirectionButton.click();
			wc.until(ExpectedConditions.elementToBeClickable(loc.lightwindow_close));
			try {
				Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), "Driving directions could not be generated");
				test2.log(LogStatus.PASS, "Pass."+""+test2.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");
			} catch (AssertionError e) {
				test2.log(LogStatus.FAIL, e.getMessage()+""+test2.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			loc.lightwindow_close.click();
			test2.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test2.log(LogStatus.ERROR, e.getMessage());
			System.out.println("Excpetion Occured.");
			e.printStackTrace();
		}
	}

	@AfterTest
	public void finish()
	{
		extent.endTest(test2);
	}

}
