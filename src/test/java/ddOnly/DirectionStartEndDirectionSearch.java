package ddOnly;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class DirectionStartEndDirectionSearch extends BrowserStackTestNGTest {

	ExtentTest test4;

	@Test
	public void check_search_for_startandend_direction() throws IOException
	{
		try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test4=extent.startTest("TC 4 - Client Side - Direction - Start and End Address Search.");
			System.out.println("TC 4 - Client Side - Direction - Start and End Address Search.");
			test4.log(LogStatus.INFO, "Scenario: Searh by start and end address.");
			System.out.println("Scenario: Searh by start and end address.");
			driver.get(WebElementLocator.ddonlyUrl);
			test4.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.ddonlyUrl);
			loc.startAddress.click();
			loc.startAddress.clear();
			loc.startAddress.sendKeys("78701");
			test4.log(LogStatus.INFO, "Test Step 2 - Enter postal code 78701 in 'Start Address'.");
			System.out.println("Test Step 2 - Enter postal code 78701 in 'Start Address'.");
			loc.endAddress.click();
			loc.endAddress.clear();
			loc.endAddress.sendKeys("84321");
			test4.log(LogStatus.INFO, "Test Step 3 - Enter postal code 84321 in 'End Address'.");
			System.out.println("Test Step 3 - Enter postal code 84321 in 'End Address'.");
			Thread.sleep(3000);
			loc.searchDirectionButton.click();
			test4.log(LogStatus.INFO, "Test Step 4 - Click on 'Get Direction' button.");
			System.out.println("Test Step 4 - Click on 'Get Direction' button.");
			try {
				wc.until(ExpectedConditions.visibilityOf(loc.clientdistanceMenu));
				test4.log(LogStatus.PASS, "Pass."+""+test4.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (Exception e) {
				test4.log(LogStatus.FAIL, e.getMessage()+""+test4.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();

			}
			test4.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test4.log(LogStatus.ERROR, e.getMessage());
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}
	}

	@AfterTest
	public void finish()
	{
		extent.endTest(test4);
	}

}
