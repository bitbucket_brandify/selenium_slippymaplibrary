package clientSide;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class GeoSendDirectionViaEmail extends BrowserStackTestNGTest {

	ExtentTest test7;

	@Test
	public void geoIp_send_direction_via_email() throws IOException
	{
		try {
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test7=extent.startTest("TC 7 - geoIP - Send Direction Via Email");
			System.out.println("TC 7 - geoIP - Send Direction Via Email");
			test7.log(LogStatus.INFO, "Scenario - Searched direction must be send to input email address.");
			System.out.println("Scenario - Searched direction must be send to input email address.");
			test7.log(LogStatus.INFO, "Open Url - "+WebElementLocator.clientSideUrl);
			System.out.println("Open Url - "+WebElementLocator.clientSideUrl);
			driver.get(WebElementLocator.clientSideUrl);
			test7.log(LogStatus.INFO, "Enter geoip '60176' in input address text box.");
			System.out.println("Enter geoip '60176' in input address text box.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("60176");
			test7.log(LogStatus.INFO, "Select 100 miles in radius.");
			System.out.println("Select 100 miles in radius.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test7.log(LogStatus.INFO, "Click on 'Search' button to get direction.");
			System.out.println("Click on 'Search' button to get direction.");
			loc.clientsearchButton.click();
			test7.log(LogStatus.INFO, "Click on 'Email' link to get window pop up.");
			System.out.println("Click on 'Email' link to get window pop up.");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//a[contains(@url,'/lite?appkey=DCD7623A-0CF8-11E4-823A-DEDE746343CC&action=emaillocatorstart&template=emaillocatorstart&uid=1078649643')]")).click();;
			wc.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("lightwindow_iframe"));
			wc.until(ExpectedConditions.elementToBeClickable(loc.userEmail));
			test7.log(LogStatus.INFO, "Enter user email address 'ssoni@where2getit.com'.");
			System.out.println("Enter user email address 'ssoni@where2getit.com'.");
			loc.userEmail.sendKeys("ssoni@where2getit.com");
			test7.log(LogStatus.INFO, "- Enter user user comment 'Automated comment'.");
			System.out.println("Enter user user comment 'Automated comment'.");
			loc.userComment.sendKeys("Automated User comment.");
			test7.log(LogStatus.INFO, "Click on 'Submit' button.");
			System.out.println("Click on 'Submit' button.");
			loc.submitButton.click();
			try {
				wc.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[contains(text(),'Confirmation')]"))));
				test7.log(LogStatus.PASS, "Pass."+""+test7.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (Exception e1) {
				test7.log(LogStatus.FAIL, e1.getMessage()+""+test7.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e1.printStackTrace();


			}
			test7.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test7.log(LogStatus.ERROR, e.getMessage()+""+test7.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}        


	}
	@AfterTest
	public void finish()
	{
		extent.endTest(test7);
	}

}
