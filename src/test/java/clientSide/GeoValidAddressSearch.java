package clientSide;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

public class GeoValidAddressSearch extends BrowserStackTestNGTest {
	ExtentTest test8;

	@Test
	public void geoip_valid_address_search() throws InterruptedException, IOException
	{

		WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
		test8=extent.startTest("TC 8 - geoIP - Valid Address Search.");
		System.out.println("TC 8 - geoIP - Valid Address Search.");
		test8.log(LogStatus.INFO, "Scenario - Check valid search result is showed for valid address.");
		System.out.println("Scenario - Searched direction must be send to input email address.");
		test8.log(LogStatus.INFO, "Open Url - "+WebElementLocator.clientSideUrl);
		System.out.println("Open Url - "+WebElementLocator.clientSideUrl);
		driver.get(WebElementLocator.clientSideUrl);
		test8.log(LogStatus.INFO, "Enter geoip '60176' in input address text box.");
		System.out.println("Enter geoip '60176' in input address text box.");
		loc.clientinputaddress.click();
		loc.clientinputaddress.clear();
		loc.clientinputaddress.sendKeys("60176");
		test8.log(LogStatus.INFO, "Select 100 miles in radius.");
		System.out.println("Select 100 miles in radius.");
		new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
		Thread.sleep(2000);
		test8.log(LogStatus.INFO, "Click on 'Search' button to get direction.");
		System.out.println("Click on 'Search' button to get direction.");
		loc.clientsearchButton.click();
		List<WebElement> li = driver.findElements(By.linkText("Email"));
		if(li.size()<=0)
		{
			test8.log(LogStatus.FAIL, ""+test8.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Fail.")	;
		}
		else{
			test8.log(LogStatus.PASS, "Pass."+""+test8.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Pass.");
		}


	}
	@AfterTest
	public void finish()
	{
		extent.endTest(test8);
	}
}
