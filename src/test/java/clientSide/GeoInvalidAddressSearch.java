package clientSide;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class GeoInvalidAddressSearch extends BrowserStackTestNGTest{
    ExtentTest test6;
	
    @Test
    public void geoCoding_invalid_address_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test6=extent.startTest("TC 6 geoCoding - Invalid Address Search.");
			System.out.println("TC 6 geoCoding - Invalid Address Search.");
			test6.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if for invalid address search.");
			System.out.println("Scenario: Light Window Pop Up must shows if for invalid address search.");
			test6.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.clientSideUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.clientSideUrl);
			driver.get(WebElementLocator.clientSideUrl);
			test6.log(LogStatus.INFO, "Enter invalid address 'abc123000' in address line.");
			System.out.println("Enter invalid address 'abc123000' in address line.");
			loc.clientinputaddress.click();
			loc.clientinputaddress.clear();
			loc.clientinputaddress.sendKeys("abc123000");
			test6.log(LogStatus.INFO, "Selected radius 100 miles.");
			System.out.println("Selected radius 100 miles.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test6.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.lightwindow_close));
			try {
				Assert.assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), 
						"No locations were found using your search criteria [ abc123000 ]. Please try another input address to search for locations.");
				test6.log(LogStatus.PASS, "Pass."+""+test6.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");

			} catch (AssertionError e) {
				test6.log(LogStatus.FAIL, e.getMessage()+""+test6.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test6.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");

		} catch (Exception e) {
			test6.log(LogStatus.ERROR, e.getMessage()+""+test6.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test6);
    }
    
    
}
