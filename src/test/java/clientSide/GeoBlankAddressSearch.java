package clientSide;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import pageObjectModel.WebElementLocator;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class GeoBlankAddressSearch extends BrowserStackTestNGTest{
    ExtentTest test5;
	
    @Test
    public void geoCoding_blank_address_search() throws InterruptedException, IOException {
    	try {
			//Initializing WebElement from Page Factory.
			WebElementLocator loc= PageFactory.initElements(driver, WebElementLocator.class);
			test5=extent.startTest("TC 5:geoCoding - Blank Address Search.");
			System.out.println("TC 5:geoCoding - Blank Address Search.");
			test5.log(LogStatus.INFO, "Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			System.out.println("Scenario: Light Window Pop Up must shows if 'Start Address' is not provided.");
			test5.log(LogStatus.INFO, "Test Step 1 - Open Url "+WebElementLocator.clientSideUrl);
			System.out.println("Test Step 1 - Open Url "+WebElementLocator.clientSideUrl);
			driver.get(WebElementLocator.clientSideUrl);
			test5.log(LogStatus.INFO, "Clear input address field for blank address search.");
			System.out.println("Clear input address field for blank address search.");
			loc.clientinputaddress.clear();
			loc.clientinputaddress.click();
			test5.log(LogStatus.INFO, "Selected radius 100 miles.");
			System.out.println("Selected radius 100 miles.");
			new Select(driver.findElement(By.name("searchradius"))).selectByVisibleText("100 miles");
			Thread.sleep(2000);
			test5.log(LogStatus.INFO, "Click on 'Search' button.");
			System.out.println("Click on 'Search' button.");
			loc.clientsearchButton.click();
			wc.until(ExpectedConditions.visibilityOf(loc.lightwindow_close));
			try {
				assertEquals(driver.findElement(By.cssSelector("#lightwindow_contents > #lightwindow_cache_data")).getText(), "Please enter an address.");
				test5.log(LogStatus.PASS, "Pass."+""+test5.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Pass.");
			} catch (AssertionError e) {
				test5.log(LogStatus.FAIL, e.getMessage()+""+test5.addScreenCapture(captureScreenMethod(dest)));
				System.out.println("Fail.");
				e.printStackTrace();
			}
			test5.log(LogStatus.INFO, "Done.");
			System.out.println("Done.");
		} catch (Exception e) {
			test5.log(LogStatus.ERROR, e.getMessage()+""+test5.addScreenCapture(captureScreenMethod(dest)));
			System.out.println("Exception Occured.");
			e.printStackTrace();
		}

        
    }
    
    @AfterTest
    public void finish() {
		extent.endTest(test5);
    }
    
    
}
